VERILATOR ?= verilator

TOP = top.sv
SIM = sim.cpp

all: verilate

obj_dir/Vtop: $(TOP) $(SIM)
	$(VERILATOR) -Wall --trace --cc $(TOP) --exe $(SIM)
	make -j4 -k -C obj_dir -f Vtop.mk Vtop

verilate: obj_dir/Vtop

lint:
	@$(VERILATOR) -Wall --lint-only $(TOP)

run: obj_dir/Vtop
	@obj_dir/Vtop

gtkwave: dump.vcd
	@gtkwave dump.vcd &

dump.vcd: run

view: top.svg
	@inkview top.svg 2> /dev/null &

top.svg: top.json
	@node $(NETLISTSVG) top.json -o $@

top.json: $(TOP)
	@yosys -q -o top.json -p proc -p opt -p "hierarchy -auto-top -libdir ." $(TOP)

clean:
	rm -rf obj_dir dump.vcd top.svg top.json

.PHONY:
	verilate run trace clean
