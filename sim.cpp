#include <iostream>
#include <verilated.h>

#if VM_TRACE
#include <verilated_vcd_c.h>
#endif

#include "Vtop.h"

#define DEFAULT_TICKS 1000

static vluint64_t main_time = 0;

double sc_time_stamp()
{
	return main_time;
}

int main(int argc, char *argv[])
{
	uint64_t max_ticks = 0;
	Vtop *top = new Vtop;

	Verilated::commandArgs(argc, argv);

	if (getenv("MAX_TICKS"))
		max_ticks = strtoull(getenv("MAX_TICKS"), NULL, 0);
	if (!max_ticks)
		max_ticks = DEFAULT_TICKS;

#if VM_TRACE
	Verilated::traceEverOn(true);
	VL_PRINTF("Tracing enabled.\n");
	VerilatedVcdC *vcd = new VerilatedVcdC;
	top->trace(vcd, 99);
	vcd->open("dump.vcd");
#endif

	while ((main_time <= 2 * max_ticks + 1) && !Verilated::gotFinish()) {
		top->eval();
		main_time++;
#if VM_TRACE
		vcd->dump(main_time);
#endif
		top->clk_i ^= 1;
	}

#if VM_TRACE
	vcd->close();
	delete vcd;
#endif

	delete top;

	return 0;
}
