module fulladder(
	input logic a_i,
	input logic b_i,
	input logic cin_i,
	output logic s_o,
	output logic cout_o
);
	assign s_o = a_i ^ b_i ^ cin_i;
	assign cout_o = (a_i & b_i) | (cin_i & (a_i ^ b_i));
endmodule

