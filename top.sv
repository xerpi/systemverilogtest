module top(
	input logic clk_i,
	output logic [31:0] x_o,
	output logic y_o,
	output logic z_o,
	output logic s_o,
	output logic cout_o
);
	assign y_o = ~x_o[0];
	assign z_o = x_o[1] | y_o;

	fulladder fa(
		.a_i(x_o[0]),
		.b_i(1),
		.cin_i(0),
		.s_o(s_o),
		.cout_o(cout_o)
	);

	always_ff @(posedge clk_i) begin
		x_o <= x_o + 1;
	end

	initial begin
		$display("Hello World");
	end
endmodule
